
# Changelog
All notable changes to this project will be documented in this file.

## [0.5.0]
### Updated
  - updated runtime dry dependencies

## [0.3.3]
### Added
  - default content type `application/json` in handler by [@valeriia.kolisnyk]

## [0.3.2]
### Added
  - autoremove thread variables before handle message (used by pipeline storage)

## [0.3.1]
### Removed
  - removed `chain` support
### Added
  - `bind_to` can take block for custom bindings
  - `pipeline` and `pipeline_id` can be used as handler params

## [0.3.0] - 2022-03-14
### Added
- `content_type` option to send message by [@valeriia.kolisnyk]
- serialization/deserialization for protobuf by [@valeriia.kolisnyk]
- `json` and `protobuf_service` switchers for handler by [@valeriia.kolisnyk]

## [0.1.1] - 2021-06-01
### Added
- `pre_publish_hooks` to configuration by [@Samuron]

### Fixed
- bindings for `notice` by [@anton.klyzhka].
- custom handlers for resource name with `-` by [@anton.klyzhka].

### Changed
- return `chain` to header and payload for back compatibility by [@anton.klyzhka].
