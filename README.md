# Strum::Esb

Useful sender RabbitMQ messages and handle them

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'strum-esb'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install strum-esb

## Usage

### Configuration

Exchanges can be customized by each message type:
```ruby
Strum::Esb.config.info_exchange = `demo.info`
Strum::Esb.config.event_exchange = `demo.events`
Strum::Esb.config.action_exchange = `demo.actions`
Strum::Esb.config.notice_exchange = `demo.notice`
```

Before publish hooks allow you to configure custom code that will be able to modify properties of the message that will be published or execute any arbitrary logic
```ruby
Strum::Esb.config.before_publish_hooks << ->(body, properties) { properties[:correlation_id] = "hello world" }
```

Before handler hooks allow you to inspect raw data before any processing has strated:
```ruby
Strum::Esb.config.before_handler_hooks << ->(deserialized_msg, delivery_info, metadata) do 
  puts "We don't even know if msg is valid JSON, this is some text from RabbitMQ"
  puts deserialized_msg
end
```

After handler hook allows you to have insight on how handler was executed:
```ruby
Strum::Esb.config.after_handler_hooks << ->(deserialized_msg, delivery_info, metadata, payload, error) do 
  puts "Message was deserialized succesfully" if payload
  puts "Error occured" if error
end
```


### Sending message:

```ruby
Strum::Esb::Action.call(`payload`, `action`, `resource`)
```

```ruby
Strum::Esb::Event.success(`payload`, `resource`, `*events`)
Strum::Esb::Event.failure(`payload`, `resource`, `*events`)
```

```ruby
Strum::Esb::Info.call(`payload`, `resource`)
```

```ruby
Strum::Esb::Notice.call(`payload`, `notice`)
Strum::Esb::Notice.call(`payload`, `notice`, `resource`)
```

### Handling message:

```ruby
require "strum/esb"

module Queues
  class Resource
    include Strum::Esb::Handler

    from_queue "resource-queue",
               bindings: {
                 events: %w[user/create user/update],
                 
               }

    def handler(payload)
      # code here ...
    end
  end
end
```

### Sending protobuf via strum-esb
1. Encode your protobuf message using `encode` method to send it.
2. Define `protobuf_service` in your handler, and disable `json`:

```ruby
require "strum/esb"
require "contracts/test_pb"

module Queues
  class Resource
    include Strum::Esb::Handler

    enable_json          false
    enable_protobuf      true
    use_protobuf_service Test::Router::Service
    from_queue "resource-queue",
               bindings: {
                 events: %w[user/create user/update],
                 
               }

    def handler(payload)
      # code here ...
    end
  end
end
```

Your rpc action must have the same name as your method in queue class, for example: if method is `action_get_user`, rpc action needs to be `ActionGetUser`.

3. Pass content type as additional param when you send Strum::Esb message:
```ruby
message = { id: 10 }
args = {
  content_type: "application/x-protobuf",
  message_class: Messages::ActionGetUser
}
Strum::Esb::Action.call(message, "action", "resource", **args)
```

4. Configure proto serialization/deserialization:
```ruby
Strum::Esb.config.serializer_conf = {
  to_proto: proc do |message_class, payload|
              ::CustomSerializer.to_protobuf(message_class, payload).to_proto
            end,
  from_proto: proc do |message_class, encoded_payload|
                rpc_instance = message_class.decode(encoded_payload)
                ::CustomSerializer.from_protobuf(rpc_instance)
              end
}
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://code.qpard.com/srtrum/strum-esb.

