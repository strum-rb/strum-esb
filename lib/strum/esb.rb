# frozen_string_literal: true

require "strum/esb/version"

require "json"
require "bunny"
require "sneakers"
require "connection_pool"
require "etc"
require "strum/patch/sneakers/queue_patch"
require "strum/esb/handler"
require "strum/esb/message"
require "strum/esb/action"
require "strum/esb/event"
require "strum/esb/notice"
require "strum/esb/info"
require "dry/configurable"
require "dry/inflector"
require "strum/esb/serializer"

module Strum
  module Esb
    extend Dry::Configurable

    setting :sneakers_workers, default: ENV.fetch("SNEAKERS_WORKERS", 1)
    setting :exchange, default: "strum.general"
    setting :info_exchange, default: "strum.info"
    setting :event_exchange, default: "strum.events"
    setting :action_exchange, default: "strum.actions"
    setting :notice_exchange, default: "strum.notice"
    setting :rabbit_channel_pool, default: begin
      ConnectionPool.new(size: 5, timeout: 5) do
        rabbit_connection = Bunny.new
        rabbit_connection.start
        rabbit_connection.create_channel
      end
    end
    setting :before_fork_hooks, default: []
    setting :after_fork_hooks, default: []
    setting :before_publish_hooks, default: []
    setting :before_handler_hooks, default: []
    setting :after_handler_hooks, default: []

    setting :serializer, default: Serializer.new
    setting :enable_json, default: true
    setting :enable_protobuf, default: false
    setting :serializer_conf, default: {
      to_proto: proc do |message_class, payload|
                  message_class.new(payload).to_proto
                end,
      from_proto: proc do |message_class, encoded_payload|
                    message_class.decode(encoded_payload)
                  end
    }

    Strum::Esb.config.before_fork_hooks << proc { DB.disconnect } if defined?(DB)

    Strum::Esb.config.after_fork_hooks << proc do
    end

    class Error < StandardError; end

    class ThreadVariablesCleaner
      def initialize(app, *args)
        @app = app
        @args = args
      end

      def call(deserialized_msg, delivery_info, metadata, handler)
        Thread.current.keys.each { |key| Thread.current[key] = nil }
        @app.call(deserialized_msg, delivery_info, metadata, handler)
      end
    end

    Sneakers.configure(
      log: $stdout,
      workers: Strum::Esb.config.sneakers_workers,
      hooks: {
        before_fork: -> { Strum::Esb.config.before_fork_hooks.each(&:call) },
        after_fork: -> { Strum::Esb.config.after_fork_hooks.each(&:call) }
      },
      exchange: Strum::Esb.config.exchange,
      exchange_type: "headers"
    )
    Sneakers.middleware.use(ThreadVariablesCleaner, nil)
    Sneakers.logger.level = Logger::INFO
  end
end
