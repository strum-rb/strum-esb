# frozen_string_literal: true

module Strum
  module Esb
    # Strum Message
    class Message
      FROZEN_EXCHANGE_OPTIONS = { durable: true }.freeze

      class << self
        def publish(exchange:, headers:, payload:, exchange_options: {}, **args)
          new(
            exchange: exchange,
            headers: headers,
            payload: payload,
            exchange_options: exchange_options,
            **args
          ).publish
        end
      end

      attr_reader :exchange,
                  :headers,
                  :payload,
                  :args,
                  :exchange_options,
                  :rabbit_channel

      def initialize(exchange:, headers:, payload:, exchange_options: {}, **args)
        @exchange         = exchange
        @headers          = headers
        @payload          = payload
        @args             = args # { content_type: "application/x-protobuf", message_class: Messages::ActionGetUser }
        # Sneakers::CONFIG[:exchange_options] = { type: "headers", durable: true, auto_delete: false, arguments: {} }
        @exchange_options = Sneakers::CONFIG[:exchange_options].merge(exchange_options).merge(FROZEN_EXCHANGE_OPTIONS)
        @rabbit_channel   = Strum::Esb.config.rabbit_channel_pool
      end

      def publish
        rabbit_exchange = rabbit_channel.with { |rabbit_channel| rabbit_channel.headers(exchange, exchange_options) }
        payload, valid_payload, properties = prepare_publication_options
        return unless valid_payload

        rabbit_exchange.publish(payload, **properties)
      end

      def prepare_publication_options
        properties = { headers: headers, content_type: args.fetch(:content_type, "application/json") }

        Strum::Esb.config.before_publish_hooks.each { |hook| hook.call(payload, properties) }
        extend_headers(properties)
        args.merge!(properties)

        # [serialized_payload, valid, properties]
        [*Strum::Esb.config.serializer.serialize(payload, args: args), properties]
      end

      def extend_headers(properties)
        properties[:headers] = {} unless properties[:headers].is_a?(Hash)
        properties[:headers]["pipeline"] ||= Thread.current[:pipeline] if Thread.current[:pipeline]
        properties[:headers]["pipeline-id"] ||= Thread.current[:pipeline_id] if Thread.current[:pipeline_id]
      end
    end
  end
end
