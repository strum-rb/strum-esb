# frozen_string_literal: true

module Strum
  module Esb
    VERSION = "0.5.0"
  end
end
