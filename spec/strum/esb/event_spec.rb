# frozen_string_literal: true

RSpec.describe Strum::Esb::Event do
  subject { described_class.new(**publish_args) }

  let(:publish_args) do
    {
      exchange: exchange,
      headers: headers.dup,
      payload: payload,
      exchange_options: exchange_options,
      **args
    }
  end

  let(:exchange) { "strum.events" }

  let(:event) { "event" }
  let(:resource) { "resource" }

  let(:headers) do
    {
      resource: resource,
      event: event,
      state: state
    }
  end

  let(:state) { nil }

  let(:payload) do
    {
      first_key: "first_value",
      second_key: "second_value",
      thirdt_key: "thirdt_value"
    }
  end

  let(:exchange_options) { {} }
  let(:args) do
    { content_type: content_type }
  end
  let(:content_type) { "application/json" }

  let(:channel) { double }
  let(:xchg) { double }

  before(:each) do
    # ============ reset sneakers config =======================================
    Sneakers.clear!
    Sneakers.configure(
      log: $stdout,
      workers: Strum::Esb.config.sneakers_workers,
      hooks: {
        before_fork: -> { Strum::Esb.config.before_fork_hooks.each(&:call) },
        after_fork: -> { Strum::Esb.config.after_fork_hooks.each(&:call) }
      },
      exchange: Strum::Esb.config.exchange,
      exchange_type: "headers"
    )
    Sneakers.middleware.use(Strum::Esb::ThreadVariablesCleaner, nil)
    Sneakers.logger.level = Logger::INFO
    # ============ reset sneakers config =======================================

    Strum::Esb.config.rabbit_channel_pool = channel

    allow(channel).to receive(:with).and_yield(channel)
    allow(channel).to receive(:headers).and_return(xchg)
    allow(xchg).to receive(:publish)
  end

  after(:each) do
    Strum::Esb.config.rabbit_channel_pool = nil
    Strum::Esb.config.before_publish_hooks = []

    Thread.current[:pipeline] = nil
    Thread.current[:pipeline_id] = nil
  end

  describe ".success" do
    let(:state) { :success }

    before do
      allow(described_class)
        .to receive(:new)
        .with(
          exchange: exchange,
          headers: headers,
          payload: payload,
          exchange_options: exchange_options,
          **args
        ).and_return(subject)

      allow(subject).to receive(:publish)
    end

    it "initialize instance" do
      described_class.success(
        payload,
        resource,
        event,
        **args
      )

      expect(described_class)
        .to have_received(:new)
        .with(
          exchange: exchange,
          headers: headers,
          payload: payload,
          exchange_options: exchange_options,
          **args
        )
    end

    it "call #publish" do
      described_class.success(
        payload,
        resource,
        event,
        exchange_options: exchange_options,
        **args
      )

      expect(subject).to have_received(:publish).once
    end
  end

  describe ".failure" do
    let(:state) { :failure }

    before do
      allow(described_class)
        .to receive(:new)
        .with(
          exchange: exchange,
          headers: headers,
          payload: payload,
          exchange_options: exchange_options,
          **args
        ).and_return(subject)

      allow(subject).to receive(:publish)
    end

    it "initialize instance" do
      described_class.failure(
        payload,
        resource,
        event,
        **args
      )

      expect(described_class)
        .to have_received(:new)
        .with(
          exchange: exchange,
          headers: headers,
          payload: payload,
          exchange_options: exchange_options,
          **args
        )
    end

    it "call #publish" do
      described_class.failure(
        payload,
        resource,
        event,
        exchange_options: exchange_options,
        **args
      )

      expect(subject).to have_received(:publish).once
    end
  end
end
